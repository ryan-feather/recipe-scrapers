from ._abstract import AbstractScraper
from ._utils import get_minutes, normalize_string, get_serves

import json
import re

class AllRecipes(AbstractScraper):
    # at least 3 iterations of page formats exist, hence tests and branches
    @classmethod
    def host(self):
        return 'allrecipes.com'


    def _ingredients_no_json(self):
        ingredients_html = self.soup.findAll('li', {'class': "checkList__line"})
        if not len(ingredients_html):
            ingredients_html =  self.soup.findAll('span',{'class': 'ingredients-item-name'})
        SKIPS = ('add all ingredients to list', 'advertisement', 'for the')
        ingredients = []
        for ingredient_raw in ingredients_html:
            normalized =  ingredient_raw.get_text(strip=True).lower()
            if len(normalized)>0 and not any([skip in normalized for skip in SKIPS]):
                ingredients.append(normalized)
        return ingredients

    def _title_no_json(self):
        return self.soup.find('h1').get_text()

    """


    def total_time(self):
        return get_minutes(self.soup.find('span', {'class': 'ready-in-time'}))


    def instructions(self):
        instructions_html = self.soup.findAll('span', {'class': 'recipe-directions__list--item'})

        return '\n'.join([
            normalize_string(instruction.get_text())
            for instruction in instructions_html
        ])
    """
    def _get_json(self):

        #print(self. soup.prettify())
        if not hasattr(self, 'json'): # avoid parsing repeatedly
            jsons = self.soup.find_all('script',{'type':"application/ld+json"})
            if not len(jsons):
                jsons = self.soup.find_all('script', {'type': r"\'application/ld+json\'"})
            decoded = None
            replacements = [('\\\n','\n'),('\\n','\n'),('\\\r','\r'),('\\r','\r'),('\\t','\t'), ("\\'", "\'"), ('\\\\"','\\"'),('\r', '' ),('\n',''), ('\\','')]# order matters!
            for jsonvals in jsons:
                text = jsonvals.get_text()
                #print(text)
                if re.search('\@type\"\s*\:\s*\"Recipe',text):
                    try:
                        decoded = json.loads(text)
                    except json.decoder.JSONDecodeError as jde:

                        preped = text
                        preped = re.sub('\\\\x[a-f0-9]+', '', preped) # json doesn do hex
                        #print(preped)
                        for reps in replacements:
                            preped = preped.replace(*reps)
                        #print(f'2 {preped}')
                        import pickle
                        with open('test.pkl', 'wb') as fp:
                            pickle.dump(preped,fp)
                        decoded = json.loads(preped)
                    finally:
                        break
                #print(text)
            if decoded is None:
                raise ValueError('Could not find recipe JSON.')
            if isinstance(decoded, list):
                for candidate in decoded:
                    if '@type' in candidate and candidate['@type'] == 'Recipe':
                        decoded = candidate
                        break
            finders = {self._title_impl, self._ingredients_impl}


            if not all([_finder(decoded) for _finder in finders]):
                if '@graph' in decoded:
                    graph = decoded['@graph']
                    for candidate in graph: # happens on class="yoast-schema-graph"
                        if candidate['@type']=='Recipe':
                            decoded = candidate
                            break

                if not all([_finder(decoded) for _finder in finders]):
                    raise ValueError(f'JSON missing keys')
            self.json = decoded
        return self.json

    def _title_impl(self, recipe):
        return recipe.get('name', None)

    def title(self):
        """

        :return: None or title
        """
        try:
            recipe = self._get_json()
            return self._title_impl(recipe)
        except ValueError:
            return self._title_no_json()

    def total_time(self):
        raise NotImplemented()
        #return get_minutes(self.soup.find('span', {'class': 'preptime'})) +\
        #       get_minutes(self.soup.find('span', {'class': 'cooktime'}))


    def _ingredients_impl(self, recipe):
        return recipe.get('recipeIngredient', None)

    def ingredients(self):
        """

        :return: ingredients or None
        """
        try:
            recipe = self._get_json()
            return self._ingredients_impl(recipe)
        except ValueError:
            return self._ingredients_no_json()

    def instructions(self):
        raise NotImplemented()
        #instructions_html = self.soup.find('div', {'class': 'instructions multiple'}).findAll('div')

        #return '\n'.join([
        #    normalize_string(instruction.get_text())
        #    for instruction in instructions_html
        #])

    def serves(self):
        try:
            _json = self._get_json()

            return get_serves(self.soup.find('section', {'class': 'recipe-ingredients-new'}).get('data-servings'))
        except ValueError:
            yieldmeta = self.soup.find('meta', {'itemprop':'recipeYield'})
            if yieldmeta is not None:
                return get_serves(yieldmeta.get('content'))
            else:
                servediv = self.soup.find('div', {'class':'recipe-adjust-servings__original-serving'})
                return get_serves(servediv.get_text())
