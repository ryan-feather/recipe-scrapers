from ._abstract import AbstractScraper
from ._utils import get_serves
import re
import json

class JSONScraper(AbstractScraper):

    def _get_json(self):

        #print(self. soup.prettify())
        if not hasattr(self, 'json'): # avoid parsing repeatedly
            jsons = self.soup.find_all('script',{'type':"application/ld+json"})
            if not len(jsons):
                jsons = self.soup.find_all('script', {'type': r"\'application/ld+json\'"})
            decoded = None
            replacements = [('\\\n','\n'),('\\n','\n'),('\\\r','\r'),('\\r','\r'),('\\t','\t'), ("\\'", "\'"), ('\\\\"','\\"'),('\r', '' ),('\n','')]# order matters!
            for jsonvals in jsons:
                text = jsonvals.get_text()
                #print(text)
                if re.search('\@type\"\s*\:\s*\"Recipe',text):
                    try:
                        decoded = json.loads(text)
                    except json.decoder.JSONDecodeError as jde:

                        preped = text
                        preped = re.sub('\\\\x[a-f0-9]+', '', preped) # json doesn do hex
                        #print(preped)
                        for reps in replacements:
                            preped = preped.replace(*reps)
                        #print(f'2 {preped}')
                        import pickle
                        with open('test.pkl', 'wb') as fp:
                            pickle.dump(preped,fp)
                        decoded = json.loads(preped)
                    finally:
                        break
                #print(text)
            if decoded is None:
                raise ValueError('Could not find recipe JSON.')
            finders = {self._title_impl, self._ingredients_impl, self._serves_impl}


            if not all([_finder(decoded) for _finder in finders]):
                if '@graph' in decoded:
                    graph = decoded['@graph']
                    for candidate in graph: # happens on class="yoast-schema-graph"
                        if candidate['@type']=='Recipe':
                            decoded = candidate
                            break
                if not all([_finder(decoded) for _finder in finders]):
                    raise ValueError(f'JSON missing keys')
            self.json = decoded
        return self.json

    def _title_impl(self, recipe):
        return recipe.get('name', None)

    def title(self):
        """

        :return: None or title
        """
        recipe = self._get_json()
        return self._title_impl(recipe)

    def total_time(self):
        raise NotImplemented()
        #return get_minutes(self.soup.find('span', {'class': 'preptime'})) +\
        #       get_minutes(self.soup.find('span', {'class': 'cooktime'}))


    def _ingredients_impl(self, recipe):
        return recipe.get('recipeIngredient', None)

    def ingredients(self):
        """

        :return: ingredients or None
        """
        recipe = self._get_json()
        return self._ingredients_impl(recipe)

    def instructions(self):
        raise NotImplemented()
        #instructions_html = self.soup.find('div', {'class': 'instructions multiple'}).findAll('div')

        #return '\n'.join([
        #    normalize_string(instruction.get_text())
        #    for instruction in instructions_html
        #])

    def _serves_impl(self, recipe):
        if 'recipeYield' in recipe:
          return recipe['recipeYield']
        elif "nutrition" in recipe:
            return recipe["nutrition"]["servingsize"]


    def serves(self):
        """Yield or None"""
        _json = self._get_json()
        return get_serves(self._serves_impl(_json))

# all using the same format, so define in a compact format


class GeniusKitchen(JSONScraper):

    @classmethod
    def host(self):
        return 'geniuskitchen.com'


class OneNineSixFlavors(JSONScraper):
    @classmethod
    def host(self):
        return '196flavors.com'

class CookieAndKate(JSONScraper):
    @classmethod
    def host(self):
        return 'cookieandkate.com'


JSON_SCRAPERS = [
    GeniusKitchen,
    OneNineSixFlavors,
    CookieAndKate,

]