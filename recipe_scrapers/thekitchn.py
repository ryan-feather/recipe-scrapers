from ._abstract import AbstractScraper
from ._utils import get_serves, normalize_string
import re
import json

class TheKitchScraper(AbstractScraper):

    @classmethod
    def host(self):
        return "thekitchn.com"

    def title(self):
        return self.soup.find('h2',{'itemprop':'name'}).get_text()

    def total_time(self):
        raise NotImplemented()
        #return get_minutes(self.soup.find('span', {'class': 'preptime'})) +\
        #       get_minutes(self.soup.find('span', {'class': 'cooktime'}))

    def ingredients(self):
        ingredients_html = self.soup.findAll('li', {'itemprop': "recipeIngredient"})

        return [normalize_string(s.get_text().strip('\\n')) for s in ingredients_html]

    def instructions(self):
        raise NotImplemented()
        #instructions_html = self.soup.find('div', {'class': 'instructions multiple'}).findAll('div')

        #return '\n'.join([
        #    normalize_string(instruction.get_text())
        #    for instruction in instructions_html
        #])

    def serves(self):
        return get_serves(self.soup.find('p',{'itemprop':'recipeYield'}).get_text())
