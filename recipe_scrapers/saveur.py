from ._abstract import AbstractScraper
from ._utils import get_minutes, normalize_string, get_serves


class Saveur(AbstractScraper):

    @classmethod
    def host(self):
        return 'saveur.com'

    def title(self):
        return self.soup.find('h1').get_text()

    def total_time(self):
        raise NotImplemented()
        #return get_minutes(self.soup.find('span', {'class': 'preptime'})) +\
        #       get_minutes(self.soup.find('span', {'class': 'cooktime'}))

    def ingredients(self):
        ingredients_html = self.soup.find('div', {'class':'ingredients'}).findAll('div')

        return [
            normalize_string(ingredient.get_text())
            for ingredient in ingredients_html
        ]

    def instructions(self):
        raise NotImplemented()
        #instructions_html = self.soup.find('div', {'class': 'instructions multiple'}).findAll('div')

        #return '\n'.join([
        #    normalize_string(instruction.get_text())
        #    for instruction in instructions_html
        #])

    def serves(self):
        return get_serves(self.soup.find('span',{'property':'recipeYield'}).get_text())